import copy
import time
import torch
import os
from tqdm import tqdm
import numpy as np
import argparse
from PIL import Image

import pretrainedmodels as ptm
from pretrainedmodels.models.senet import SENet, SEBottleneck
from torchvision import datasets, models, transforms
from pretrainedmodels.models.xception import Xception

from torchvision.transforms import functional as F
from torch import nn, random
from torch.optim import lr_scheduler


# os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
#
# os.environ["CUDA_VISIBLE_DEVICES"]="1"
torch.cuda.manual_seed_all(42)
torch.manual_seed(1254)


class ResizeMy(object):
    def __init__(self, size):
        self.size = (int(size), int(size))

    @staticmethod
    def get_params(img, output_size):
        pass

    def __call__(self, img):
        w, h = img.size
        while self.size[0] > h or self.size[1] > w:
            w *= 2
            h *= 2
            img = F.resize(img, (w, h))

        return img


def softmax(x):
    """Compute softmax values for each sets of scores in x."""
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum(axis=0)


def inference(model, dir_test, model_name, file_name, image_size=224, image_count=16):
    
    # model = Xception(num_classes=2)
    # model.last_linear = model.fc
    model.load_state_dict(torch.load(model_name))
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    model_ft = model.to(device)
    model.eval()
    files = os.listdir(dir_test)
    lines = ['0,2\n']
    count_1 = 0
    predict_batch = np.zeros((image_count, 3, image_size, image_size), dtype=np.float64)
    tran = transforms.Compose([
                           ResizeMy(image_size+1),
                           transforms.RandomCrop(image_size),
                           transforms.RandomHorizontalFlip(),
                           transforms.RandomVerticalFlip(),
                           transforms.ToTensor(),
                           transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])
    with torch.no_grad():
        for file_ in tqdm(files):
            index = 0

            for i in range(image_count):
                im = Image.open(os.path.join(dir_test, file_))
                im = tran(im)

                predict_batch[index] = im
                index += 1
            predict_batch_torch = torch.from_numpy(predict_batch)
            predict_batch_torch = predict_batch_torch.to(device).float()
            res = model_ft(predict_batch_torch)
            res_np = res.cpu().numpy()
            res_np = np.array([softmax(x) for x in res_np])
            res_np = np.mean(res_np, axis=0)

            ans = res_np[1]

            if res_np[0] < 0.5:
                # ans = 1
                count_1 += 1
            # print(file_, ans)
            lines.append('{},{}\n'.format(file_, ans))
            # print(file_, ans)
    print('count 1 is ', count_1)
    with open(file_name, 'w', encoding='utf8') as f:
        for line in lines:
            f.write(line)
    return count_1
    
  
def get_items_from_file(file_):
    f_line = True
    res = {}
    with open(file_) as f:
        for line in f:
            if f_line:
                f_line = False
                continue
            file_name, value = line.strip().split(',')
            res[file_name] = float(value)
    return res

    
def merge_submissions(subs, dst_file, weights=None):
    if weights is None:
        weights = [1/len(subs)] * len(subs)

    items = [get_items_from_file(x) for x in subs]
    result = {}
    counter_1 = 0
    for key in tqdm(items[0].keys()):
        result_value = 0
        for i, item in enumerate(items):
            result_value += weights[i] * item[key]
        # result[key] = 0 if result_value > 0.5 else 1
        result[key] = result_value
        if result[key] == 1:
            counter_1 += 1
    with open(dst_file, 'w', encoding='utf8') as f:
        f.write('0,2\n')
        for key in result:
            f.write('{},{}\n'.format(key, result[key]))
    return counter_1
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Spoof faces inference')
    parser.add_argument('--data_dir', default=r"/test", type=str,
                        help='directory with files')
    parser.add_argument('--images_count', default=24, type=int, 
                        help='count images for TTA')
    parser.add_argument('--dst_dir', default=r"/output", type=str,
                        help='destination directory')

    args = parser.parse_args()    
        
    for i in range(1, 6):
        print('start fold {}/{} SENet\n'.format(i, 5))
        model = SENet(SEBottleneck, [3, 8, 36, 3], groups=64, reduction=16,
                  dropout_p=0.2, num_classes=2)
        inference(model, args.data_dir, 'pytorch_model_senet_{}.h5'.format(i), 
                  os.path.join(args.dst_dir, 'pytorch_senet_cv_{}.csv'.format(i)), 
                  image_count=args.images_count)
    merge_submissions([os.path.join(args.dst_dir, 'pytorch_senet_cv_{}.csv'.format(i)) for i in range(1,6)],
                       os.path.join(args.dst_dir, 'result_submission_senet.csv'))
    print('Done. The first result submission is result_submission_senet.csv')
    
    for i in range(1, 6):
        print('start fold {}/{}\n Xception'.format(i, 5))
        model = Xception(num_classes=2)
        model.last_linear = model.fc
        inference(model, args.data_dir, 'pytorch_model_xception_{}.h5'.format(i), 
                  os.path.join(args.dst_dir, 'pytorch_xception_cv_{}.csv'.format(i)), 
                  image_count=args.images_count, image_size=299)
                  
    merge_submissions([os.path.join(args.dst_dir, 'pytorch_xception_cv_{}.csv'.format(i)) for i in range(1,6)],
                       os.path.join(args.dst_dir, 'result_submission_xception.csv'))
    print('Done. The second result submission is result_submission_xception.csv')
    
    merge_submissions([os.path.join(args.dst_dir, 'result_submission_xception.csv'), 
                      os.path.join(args.dst_dir, 'result_submission_senet.csv')],
                      os.path.join(args.dst_dir, 'result_submission.csv'))
                       
    print('The last result submission is result_submission.csv')
    
    tmp_subms = [os.path.join(args.dst_dir, 'pytorch_senet_cv_{}.csv'.format(i)) for i in range(1,6)]
    tmp_subms.extend([os.path.join(args.dst_dir, 'pytorch_xception_cv_{}.csv'.format(i)) for i in range(1,6)])  
    for el in tmp_subms:
        os.remove(el)    
