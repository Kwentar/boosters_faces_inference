import copy
import time
import argparse
import torch
from torchvision import datasets, models, transforms
from pretrainedmodels.models.senet import SENet, SEBottleneck
from sklearn.metrics import roc_auc_score
import numpy as np

import os
from torchvision.transforms import functional as F

from torch import nn
from torch.optim import lr_scheduler
from tqdm import tqdm
from sklearn.metrics import roc_curve


os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152

os.environ["CUDA_VISIBLE_DEVICES"]="1"
torch.cuda.manual_seed_all(42)
torch.manual_seed(1254)


class ResizeMy(object):
    def __init__(self, size):
        self.size = (int(size), int(size))

    @staticmethod
    def get_params(img, output_size):
        pass

    def __call__(self, img):
        w, h = img.size
        while self.size[0] > h or self.size[1] > w:
            w *= 2
            h *= 2
            img = F.resize(img, (w, h))

        return img

image_size = 224
data_transforms = {
    'train': transforms.Compose([
        ResizeMy(image_size+1),
        transforms.RandomCrop(image_size),
        transforms.RandomHorizontalFlip(),
        transforms.RandomVerticalFlip(),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ]),
    'val': transforms.Compose([
        ResizeMy(image_size+1),
        transforms.RandomCrop(image_size),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ]),
}


def train_model(model, criterion, optimizer, scheduler, dataloaders, 
                dataset_sizes, device, model_name, num_epochs=25):
    since = time.time()

    best_model_wts = copy.deepcopy(model.state_dict())
    best_auc =  0.0
    best_epoch = -1
    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        for phase in ['train', 'val']:
            if phase == 'train':
                scheduler.step()
                model.train()  
            else:
                model.eval()   

            running_loss = 0.0
            running_corrects = 0
            all_inputs = []
            all_preds = []
            for inputs, labels in tqdm(dataloaders[phase]):
                all_inputs.extend(list(labels.numpy()))
                inputs = inputs.to(device)
                labels = labels.to(device)
                
                optimizer.zero_grad()

                with torch.set_grad_enabled(phase == 'train'):
                    outputs = model(inputs)
                    _, preds = torch.max(outputs, 1)
                    all_preds.extend(list(preds.cpu().numpy()))
                    loss = criterion(outputs, labels)

                    if phase == 'train':
                        loss.backward()
                        optimizer.step()
            
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data)
                

            epoch_loss = running_loss / dataset_sizes[phase]
            epoch_acc = running_corrects.double() / dataset_sizes[phase]
            epoch_auc = roc_auc_score(np.array(all_inputs), np.array(all_preds))
            fpr, tpr, threshold = roc_curve(np.array(all_inputs), np.array(all_preds), pos_label=1)
            fnr = 1 - tpr
            eer_threshold = threshold[np.nanargmin(np.absolute((fnr - fpr)))]
            epoch_eer = fpr[np.nanargmin(np.absolute((fnr - fpr)))]
            
            print('{} Loss: {:.4f} Acc: {:.4f} ROC AUC: {:.8f} EER: {:.8f}'.format(
                phase, epoch_loss, epoch_acc, epoch_auc, epoch_eer))

            if phase == 'val' and epoch_auc >= best_auc:
                best_epoch = epoch
                print('NEW BEST ONE {} {}'.format(epoch, epoch_auc))
                best_auc = epoch_auc
                best_model_wts = copy.deepcopy(model.state_dict())
                torch.save(best_model_wts, model_name)
        print()

    time_elapsed = time.time() - since
    print('Best epoch is {}'.format(best_epoch))
    print('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
    print('Best val Acc: {:4f}'.format(best_auc))


    

def train_main(dir_, model_name):
    image_datasets = {x: datasets.ImageFolder(os.path.join(dir_, x),
                                              data_transforms[x])
                      for x in ['train', 'val']}
    dataloaders = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size=20,
                                                  shuffle=True, num_workers=4) for x in ['train', 'val']}
    dataset_sizes = {x: len(image_datasets[x]) for x in ['train', 'val']}
    class_names = image_datasets['train'].classes


    model = SENet(SEBottleneck, [3, 8, 36, 3], groups=64, reduction=16,
                    dropout_p=0.2, num_classes=1000)
    model.load_state_dict(torch.load(r'senet154-c7b49a05.pth'))
 
    model.last_linear = torch.nn.Linear(model.last_linear.in_features, 2)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model_ft = model.to(device)

    criterion = nn.CrossEntropyLoss()
    
    optimizer_ft = torch.optim.Adam(model_ft.parameters(), lr=0.0001)

    exp_lr_scheduler = lr_scheduler.StepLR(optimizer_ft, step_size=5, gamma=0.1)
    
    train_model(model_ft, criterion, optimizer_ft, exp_lr_scheduler, 
                dataloaders, dataset_sizes, device, model_name, num_epochs=30)
                           
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Spoof faces train')
    parser.add_argument('--data_dir', default=r"train_data/", type=str,
                        help='directory with files, contains two folder: train and val')
    parser.add_argument('--model_name', default='pytorch_model_senet.h5', type=str, 
                        help='count images for TTA')

    args = parser.parse_args()    

    train_main(args.data_dir, args.model_name)