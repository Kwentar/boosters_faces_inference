Docker container based on https://github.com/anibali/docker-pytorch

build sample: docker build -t boosters_inference .


run template: docker run --runtime=nvidia -v <test_dir>:/test/ -v <output_dir>:/output boosters_inference

run sample: docker run --runtime=nvidia -v "/home/data/datasets/boosters/test":/test/ -v $PWD:/output boosters_inference

Notice: --runtime=nvidia is necessary


--------------------

Inference example:

python inference.py --data_dir ../test_images/

Train example:

python train.py --data_dir ../train_images/ --model_name tmp.h5

Folder ../train_images/ must have two subfolders with names 'train' and 'val'

-------------------

File description.pdf has the description of solution