FROM anibali/pytorch:cuda-9.0
COPY . /app
RUN pip install tqdm pretrainedmodels --no-cache-dir 
CMD python /app/inference.py